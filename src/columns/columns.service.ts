import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CardsRepository } from "src/cards/cards.repository";
import { CardEntity } from "src/cards/entities/card.entity";
import { ColumnsRepository } from "./columns.repository";
import { CreateColumnDto } from "./dto/create-column.dto";
import { UpdateColumnDto } from "./dto/update-column.dto";
import { ColumnEntity } from "./entities/column.entity";

@Injectable()
export class ColumnsService {
  constructor(
    private readonly columnsRepository: ColumnsRepository,
    private readonly cardsRepository: CardsRepository
  ) {}

  async findAll(): Promise<ColumnEntity[]> {
    return this.columnsRepository.find();
  }

  async findOne(id: ColumnEntity['id']): Promise<ColumnEntity> {
    return this.columnsRepository.findOneOrFail(id);
  }

  async create(dto: CreateColumnDto): Promise<ColumnEntity> {
    return this.columnsRepository.save(dto);
  }

  async update(
    id: ColumnEntity['id'],
    dto: UpdateColumnDto
  ): Promise<ColumnEntity> {
    return this.columnsRepository.save({ id, ...dto });
  }

  async delete(id: ColumnEntity['id']): Promise<boolean> {
    await this.columnsRepository.delete(id);
    return true;
  }

  async getAllCards(columnId: ColumnEntity['id']): Promise<CardEntity[]> {
    return this.cardsRepository.find({ where: {columnId: columnId} });
  }
}