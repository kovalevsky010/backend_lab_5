import { IsUUID } from 'class-validator';
import { CardEntity } from 'src/cards/entities/card.entity';
import { BaseEntity } from 'src/common/entities/base.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';


@Entity('columns')
export class ColumnEntity extends BaseEntity {
  @IsUUID()
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column('varchar')
  name!: string;

  @OneToMany(() => CardEntity, cards => cards.column)
  cards?: CardEntity[];
}
