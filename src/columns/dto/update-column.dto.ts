import { IsOptional, IsString } from "class-validator";
import { ColumnEntity } from "../entities/column.entity";

export class UpdateColumnDto {
  @IsOptional()
  @IsString()
  name?: ColumnEntity['name'];
}
