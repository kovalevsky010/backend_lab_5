import { IsDefined, IsString } from "class-validator";
import { ColumnEntity } from "../entities/column.entity";

export class CreateColumnDto {
  @IsString()
  @IsDefined()
  name!: ColumnEntity['name'];
}
