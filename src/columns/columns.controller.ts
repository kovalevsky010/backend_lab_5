import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { CardEntity } from "src/cards/entities/card.entity";
import { ColumnsService } from "./columns.service";
import { CreateColumnDto } from "./dto/create-column.dto";
import { UpdateColumnDto } from "./dto/update-column.dto";
import { ColumnEntity } from "./entities/column.entity";

@Controller('/columns')
export class ColumnsController {
  constructor(private readonly columnsService: ColumnsService) { }

  @Get()
  async findAll(): Promise<ColumnEntity[]> {
    return this.columnsService.findAll();
  }

  @Get(':id')
  async findOne(
    @Param('id') id: ColumnEntity['id']
  ): Promise<ColumnEntity> {
    return this.columnsService.findOne(id);
  }

  @Post()
  async create(
    @Body() dto: CreateColumnDto
  ): Promise<ColumnEntity> {
    return this.columnsService.create(dto);
  }

  @Patch(':id')
  async update(
    @Param('id') id: ColumnEntity['id'],
    @Body() dto: UpdateColumnDto
  ): Promise<ColumnEntity> {
    return this.columnsService.update(id, dto);
  }

  @Delete(':id')
  async delete(
    @Param('id') id: ColumnEntity['id'],
  ): Promise<boolean> {
    return this.columnsService.delete(id);
  }

  @Get('/:columnId/cards')
  async getAllCards(
    @Param('columnId') columnId: ColumnEntity['id']
  ): Promise<CardEntity[]> {
    return this.columnsService.getAllCards(columnId);
  }
}
