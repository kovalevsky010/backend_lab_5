import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CardsModule } from "src/cards/cards.module";
import { CardsRepository } from "src/cards/cards.repository";
import { ColumnsController } from "./columns.controller";
import { ColumnsRepository } from "./columns.repository";
import { ColumnsService } from "./columns.service";
import { ColumnEntity } from "./entities/column.entity";


@Module({
  imports: [
    CardsModule,
    TypeOrmModule.forFeature([
      ColumnEntity,
      ColumnsRepository,
      CardsRepository
    ]),
  ],
  controllers: [
    ColumnsController
  ],
  providers: [
    ColumnsService,
  ],
})
export class ColumnsModule {}
