import { IsDefined, IsString, IsUUID } from "class-validator";
import { ColumnEntity } from "src/columns/entities/column.entity";
import { CardEntity } from "../entities/card.entity";

export class CreateCardDto {
  @IsDefined()
  @IsString()
  name!: CardEntity['name'];

  @IsDefined()
  @IsUUID()
  columnId!: ColumnEntity['id'];
}
