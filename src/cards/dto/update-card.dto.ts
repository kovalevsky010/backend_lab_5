import { IsDefined, IsOptional, IsString, IsUUID } from "class-validator";
import { ColumnEntity } from "src/columns/entities/column.entity";
import { CardEntity } from "../entities/card.entity";

export class UpdateCardDto {
  @IsOptional()
  @IsString()
  name?: CardEntity['name']

  @IsOptional()
  @IsUUID()
  columnId?: ColumnEntity['id'];
}
