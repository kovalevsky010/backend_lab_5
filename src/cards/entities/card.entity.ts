import { IsDefined, IsUUID } from 'class-validator';
import { ColumnEntity } from 'src/columns/entities/column.entity';
import { BaseEntity } from 'src/common/entities/base.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';


@Entity('cards')
export class CardEntity extends BaseEntity {
  @IsUUID()
  @IsDefined()
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @IsUUID()
  @Column('varchar')
  columnId!: ColumnEntity['id'];

  @Column('varchar')
  name!: string;

  // relations
  @ManyToOne(() => ColumnEntity, column => column.cards, {
    onDelete: 'CASCADE',
  })
  column!: ColumnEntity;
}
