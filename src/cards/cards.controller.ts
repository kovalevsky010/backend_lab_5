import { Body, Controller, Delete, Get, Param, Patch, Post } from "@nestjs/common";
import { CardsService } from "./cards.service";
import { CreateCardDto } from "./dto/create-card.dto";
import { UpdateCardDto } from "./dto/update-card.dto";
import { CardEntity } from "./entities/card.entity";


@Controller('/cards')
export class CardsController {
  constructor(private readonly cardsService: CardsService) {}

  @Get()
  async findAll(): Promise<CardEntity[]> {
    return this.cardsService.findAll();
  }

  @Get(':id')
  async findOne(
    @Param('id') id: CardEntity['id']
  ): Promise<CardEntity> {
    return this.cardsService.findOne(id);
  }

  @Post()
  async create(
    @Body() dto: CreateCardDto
  ): Promise<CardEntity> {
    return this.cardsService.create(dto);
  }

  @Patch(':id')
  async update(
    @Param('id') id: CardEntity['id'],
    @Body() dto: UpdateCardDto
  ): Promise<CardEntity> {
    return this.cardsService.update(id, dto);
  }

  @Delete(':id')
  async delete(
    @Param('id') id: CardEntity['id'],
  ): Promise<boolean> {
    return this.cardsService.delete(id);
  }
}