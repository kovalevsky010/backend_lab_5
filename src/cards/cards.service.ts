import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CardsRepository } from "./cards.repository";
import { CreateCardDto } from "./dto/create-card.dto";
import { UpdateCardDto } from "./dto/update-card.dto";
import { CardEntity } from "./entities/card.entity";


@Injectable()
export class CardsService {
  constructor(
    private readonly cardsRepository: CardsRepository
  ) {}

  async findAll(): Promise<CardEntity[]> {
    return this.cardsRepository.find();
  }

  async findOne(id: CardEntity['id']): Promise<CardEntity> {
    return this.cardsRepository.findOneOrFail(id);
  }

  async create(dto: CreateCardDto): Promise<CardEntity> {
    return this.cardsRepository.save(dto);
  }

  async update(
    id: CardEntity['id'],
    dto: UpdateCardDto
  ): Promise<CardEntity> {
    return this.cardsRepository.save({ id, ...dto });
  }

  async delete(id: CardEntity['id']): Promise<boolean> {
    await this.cardsRepository.delete(id);
    return true;
  }
}