import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ColumnEntity } from "src/columns/entities/column.entity";
import { CardsController } from "./cards.controller";
import { CardsRepository } from "./cards.repository";
import { CardsService } from "./cards.service";
import { CardEntity } from "./entities/card.entity";

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CardEntity,
      CardsRepository,
    ]),
  ],
  controllers: [
    CardsController,
  ],
  providers: [
    CardsService,
  ],
})
export class CardsModule {}