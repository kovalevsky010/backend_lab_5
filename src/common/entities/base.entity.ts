import { CreateDateColumn, UpdateDateColumn } from "typeorm";

export class BaseEntity {
  @CreateDateColumn()
  updatedAt: string;

  @UpdateDateColumn()
  createdAt: string;
}
